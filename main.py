import matplotlib.pyplot as plt
import numpy as np
import logging
import datetime

# Норма обучения:
eta = 0.3
'''
    Ожидаемые значения функции 
    Значения для примера из методички для самопроверки: [0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0]
    Ниже результат функции варианта 18 - ¬((X1∨X2)∧X3∧X4) [1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0]
'''
f = [1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0]
x0 = 1
# Вектор xi (от 0 до 4)
x_table = [
    [x0, 0, 0, 0, 0],
    [x0, 0, 0, 0, 1],
    [x0, 0, 0, 1, 0],
    [x0, 0, 0, 1, 1],
    [x0, 0, 1, 0, 0],
    [x0, 0, 1, 0, 1],
    [x0, 0, 1, 1, 0],
    [x0, 0, 1, 1, 1],
    [x0, 1, 0, 0, 0],
    [x0, 1, 0, 0, 1],
    [x0, 1, 0, 1, 0],
    [x0, 1, 0, 1, 1],
    [x0, 1, 1, 0, 0],
    [x0, 1, 1, 0, 1],
    [x0, 1, 1, 1, 0],
    [x0, 1, 1, 1, 1],
]


# Запись вывода программы в файл в директории content/
def set_logger():
    file_handler = logging.FileHandler(filename='content/' + number_task + 'lab_output.log', encoding='utf-8')
    logging.basicConfig(format='[%(levelname)-10s] %(asctime)-25s - %(message)s', handlers=[file_handler],
                        level=logging.INFO)


def net_compute(w: list, vector_x: list):
    net = w[0]
    for i in range(1, len(vector_x)):
        net += w[i] * vector_x[i]
    return net


'''
        Коррекция веса в случае пороговой ФА
'''


def threshold_fa_correction(delta, x):
    return eta * delta * x


'''
        Коррекция веса в случае сигмоидальной ФА
        Исходная функция f(net) = 1/2 * (tahn(net) + 1)
        Функция производной в diff
'''


def sigmoidal_fa_correction(delta, net, x):
    diff = 1 / (2 * (np.cosh(net) ** 2))
    return delta * diff * x


# Вычисление расстояния Хемминга E(k)
def e_compute(y: list):
    e = 0
    for i in range(15):
        if f[i] != y[i]:
            e += 1
    return e


# Функция построения графика E(k), k - номер эпохи
def e_plotting(e: list, n, number_task):
    k = []
    for i in range(0, n):
        k.append(i)
    fig = plt.figure()
    plt.plot(k, e)
    plt.show()
    fig.savefig('content/' + str(number_task) + 'lab.png')


if __name__ == '__main__':
    number_task = input("Введите, пожалуйста, номер задания (1, 2): ")
    w = [0, 0, 0, 0, 0]
    y = [0] * 16
    e = []
    number_epoch = 0  # Количество эпох
    current_e = e_compute(y)  # Переменная для записи текущего значения E(k)
    logger = logging.getLogger(__name__)
    set_logger()
    while current_e:
        number_epoch += 1
        logger.info("Эпоха обучения: %s", number_epoch)
        for i in range(16):
            vector_x = x_table[i]
            net = net_compute(w, vector_x)
            if number_task == 1:  # результат выходного вектора в зависимости от ФА Нс
                if net >= 0:
                    y[i] = 1
                else:
                    y[i] = 0
            else:
                if net >= 0.5:
                    y[i] = 1
                else:
                    y[i] = 0

            delta = f[i] - y[i]
            '''
            Ниже представлен рассчет корректировки весового вектора
            Δw - delta_w
            '''
            for j in range(5):
                if number_task == 1:
                    delta_w = threshold_fa_correction(delta, vector_x[j])
                else:
                    delta_w = sigmoidal_fa_correction(delta, net, vector_x[j])
                w[j] += delta_w
            logger.info("\t Шаг обучения: %s; "
                        "Вектор весовых коэффициентов: %s;"
                        " Реальный выход: %s", i, w, y)

        current_e = e_compute(y)
        e.append(current_e)
        logger.info("Вектор весов W: %s; Выходной сигнал: %s; Суммарная ошибка: %s", w, y, current_e)
    e_plotting(e, number_epoch, number_task)
